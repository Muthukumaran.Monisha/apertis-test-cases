metadata:
  name: connman-pan-network-access-reconnect
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test ConnMan support for bluetooth PAN Network Access Profile.
                This test will test that after restarting the Bluetooth tethering 
                from the phone side the internet access from Apertis to a bluetooth
                device sharing its internet connection is maintained."

  resources:
    - "A USB Bluetooth adapter."
    - "A phone supporting PAN NAP. A computer can also be used to share its
       internet connection."

  macro_install_packages_preconditions: apertis-tests chaiwala-tests python3-dbus python3-gi
  pre-conditions:
    - "Make sure an Internet connection is active on the phone and can be shared.
       The steps are phone specific."
    - "Ensure the bluetooth adapter is detected in Apertis: In VirtualBox menu,
       select \"Devices > USB\" and check the entry corresponding to the selected
       bluetooth adapter."
    - "Please note that connman disables bluetooth by default on a fresh image.
       If it's already enabled, connmanctl will give an \"In progress\" error that
       may be ignored. To enable the device:"
    - $ connmanctl enable bluetooth
    - "Start simple agent:"
    - $ /usr/lib/chaiwala-tests/bluez/simple-agent -y
    - "Pair both devices initiating from the phone side"
    - "Pay attention to the simple-agent window as it requires input during
       the pairing process."

  expected:
    - "At this point, you should be able to navigate on the internet from Apertis.
       For example:"
    - |
        >user@apertis:~$ ping www.google.fr
        PING www.google.fr (216.58.198.163) 56(84) bytes of data.
        64 bytes from lhr25s10-in-f3.1e100.net (216.58.198.163): icmp_seq=1 ttl=50 time=62.0 ms
    - "Start the web browser from the Applications menu and display a web page."
    - "Specifying a DNS should not be necessary, it can be done as follow:"
    - $ connmanctl config bluetooth_aabbccddeeff_feedbeefcafe --nameservers 8.8.8.8
    - "After toggling the bluetooth tethering option from ON-OFF to ON the connection should be 
       re-established i.e. the auto-connect feature should work as expected" 
  notes:
    - "Warning: Connman changes are persistent (over reboot!). After finishing
       testing, it might be wise to perform the dhcp test to ensure that the
       network is in a sensible state."
    - "For ALL tests the enable step will give an \"In progress\" error if the
       device is already enabled. This is to be expected."

  post-conditions:
    - "Disconnect using connmanctl:"
    - $ connmanctl disconnect bluetooth_aabbccddeeff_feedbeefcafe
    - "Enable ethernet again if it was disabled"
    - $ connmanctl enable ethernet

run:
  steps:
    - "To ensure effective usage of bluetooth and not ethernet, one can use:"
    - $ connmanctl disable ethernet
    - "List connman services and Check no other network technologies is in use
       with:"
    - $ connmanctl services
    - "From the result select the bluetooth service which matches both the adapter
       and phone addresses. If connections succeeds, connmanctl will display
       Connected bluetooth_aabbccddeeff_feedbeefcafe:"
    - $ connmanctl connect bluetooth_aabbccddeeff_feedbeefcafe
    - |
        >Connected bluetooth_aabbccddeeff_feedbeefcafe
    - "Control ip routing goes through PAN interface. IP addresses can vary, but
       the default gateway should be bnepX or enXaabbccddeeff:"
    - $ ip route
    - |
        >default via 192.168.2.1 dev bnep0
        192.168.2.0/24 dev bnep0  proto kernel  scope link  src 192.168.2.3
        192.168.2.1 dev bnep0  scope link
    - "At this point an internet connection is setup on the apertis side from the phone"
    - "Now on the phone side toggle the bluetooth tethering option from the settings menu from the initial ON state 
       to OFF state and back to ON state"
